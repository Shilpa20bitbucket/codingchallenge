/* 
Purpose: Departments and Employees DML for Widget Ltd.
Author: Shilpa
Date: 23-03-2020
*/
SET DEFINE OFF;

INSERT INTO departments (
    department_name,
    location
) VALUES (
    'Management',
    'London'
);

INSERT INTO departments (
    department_name,
    location
) VALUES (
    'Engineering',
    'Cardiff'
);

INSERT INTO departments (
    department_name,
    location
) VALUES (
    'Research & Development',
    'Edinburgh'
);

commit;
INSERT INTO departments (
    department_name,
    location
) VALUES (
    'Sales',
    'Belfast'
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'John Smith',
    'CEO',
    '',
    to_date('01/01/95','dd/mm/yy'),
    100000,
    1
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Jimmy Willis',
    'Manager',
    90001,
    to_date('23/09/03','dd/mm/yy'),
    52500,
    4
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Roxy Jones',
    'Salesperson',
    90002,
    to_date('11/02/17','dd/mm/yy'),
    35000,
    4
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Selwyn Field',
    'Salesperson',
    90003,
    to_date('20/05/15','dd/mm/yy'),
    32000,
    4
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'David Hallett',
    'Engineer',
    90006,
    to_date('17/04/18','dd/mm/yy'),
    40000,
    2
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Sarah Phelps',
    'Manager',
    90001,
    to_date('21/03/15','dd/mm/yy'),
    45000,
    2
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Louise Harper',
    'Engineer',
    90006,
    to_date('01/01/13','dd/mm/yy'),
    47000,
    2
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Tina Hart',
    'Engineer',
    90009,
    to_date('28/07/14','dd/mm/yy'),
    45000,
    3
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Gus Jones',
    'Manager',
    90001,
    to_date('15/05/18','dd/mm/yy'),
    50000,
    3
);

INSERT INTO employees (
    employee_name,
    job_title,
    manager_id,
    date_hired,
    salary,
    department_id
) VALUES (
    'Mildred Hall',
    'Secretary',
    90001,
    to_date('12/10/96','dd/mm/yy'),
    35000,
    1
);

SET DEFINE ON;

COMMIT;