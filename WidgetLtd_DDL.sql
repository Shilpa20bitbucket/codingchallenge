/* 
Purpose: Departments and Employees Data Structure Creation for Widget Ltd.
Author: Shilpa
Date: 23-03-2020
*/
CREATE TABLE departments (
    department_id     NUMBER(5)
        GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 )
        CONSTRAINT department_id_pk PRIMARY KEY,
    department_name   VARCHAR2(50) NOT NULL,
    location          VARCHAR2(50) NOT NULL
);

CREATE TABLE employees (
    employee_id     NUMBER(10)
        GENERATED ALWAYS AS IDENTITY ( START WITH 90001 INCREMENT BY 1 )
        CONSTRAINT employee_id_pk PRIMARY KEY,
    employee_name   VARCHAR2(50) NOT NULL,
    job_title       VARCHAR2(50) NOT NULL,
    manager_id      NUMBER(10),
    date_hired      DATE NOT NULL,
    salary          NUMBER(10) NOT NULL,
    department_id   NUMBER(5) NOT NULL
        CONSTRAINT department_id_fk
            REFERENCES departments ( department_id )
                ON DELETE CASCADE
);