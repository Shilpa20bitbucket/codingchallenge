CREATE OR REPLACE PACKAGE widgetltd_maintenance 
/*
Purpose: This package provides the below maintenance facilities for Widget Limited:
        1. Creation of Employee
        2. Allow Salary to be increased/decreased by a percentage
        3. Transfer Employee to a different department
        4. Return Salary of an employee
Author: Shilpa Dhiman
Date: 24-04-2020
*/
AS
-- Create Employee    
    PROCEDURE add_employee (
        e_name         employees.employee_name%TYPE,
        e_job_title    employees.job_title%TYPE,
        e_manager      employees.manager_id%TYPE,
        e_date_hired   employees.date_hired%TYPE,
        e_salary       employees.salary%TYPE,
        e_dept         employees.department_id%TYPE
    );
--Increment/Decrement Salary by a percentage

    PROCEDURE update_salary (
        e_id                  employees.employee_id%TYPE,
        increase_percentage   NUMBER,
        decrease_percentage   NUMBER
    );
-- Transfer Employee to different Department

    PROCEDURE update_dept (
        e_id     employees.employee_id%TYPE,
        e_dept   employees.department_id%TYPE
    );
-- Return Salary of an employee

    FUNCTION return_salary (
        e_id IN employees.employee_id%TYPE
    ) RETURN employees.salary%TYPE;

END widgetltd_maintenance;
/

CREATE OR REPLACE PACKAGE BODY widgetltd_maintenance AS
    PROCEDURE add_employee (
        e_name         employees.employee_name%TYPE,
        e_job_title    employees.job_title%TYPE,
        e_manager      employees.manager_id%TYPE,
        e_date_hired   employees.date_hired%TYPE,
        e_salary       employees.salary%TYPE,
        e_dept         employees.department_id%TYPE
    ) AS
    BEGIN
        INSERT INTO employees (
            employee_name,
            job_title,
            manager_id,
            date_hired,
            salary,
            department_id
        ) VALUES (
            e_name,
            e_job_title,
            e_manager,
            e_date_hired,
            e_salary,
            e_dept
        );

        COMMIT;
    END add_employee;

    PROCEDURE update_salary (
        e_id                  employees.employee_id%TYPE,
        increase_percentage   NUMBER,
        decrease_percentage   NUMBER
    ) AS
        e_invalid_update EXCEPTION;
        e_invalid_percentage EXCEPTION;
    BEGIN
        IF ( increase_percentage < 0 OR decrease_percentage < 0 ) THEN
            RAISE e_invalid_percentage;
        ELSIF ( increase_percentage IS NULL OR increase_percentage = 0 ) THEN
            UPDATE employees
            SET
                salary = salary - ( salary * decrease_percentage / 100 )
            WHERE
                employee_id = e_id;

            COMMIT;
        ELSIF ( decrease_percentage IS NULL OR decrease_percentage = 0 ) THEN
            UPDATE employees
            SET
                salary = salary + ( salary * increase_percentage / 100 )
            WHERE
                employee_id = e_id;

            COMMIT;
        ELSE
            RAISE e_invalid_update;
        END IF;
    EXCEPTION
        WHEN e_invalid_update THEN
            dbms_output.put_line('Salary cannot be increased and decreased at the same time! Please enter either increase percentage or decreased percentage.'
            );
        WHEN e_invalid_percentage THEN
            dbms_output.put_line('The percentage should be a positive number!');
        WHEN no_data_found THEN
            dbms_output.put_line('The entered employee does not exist!');
        WHEN OTHERS THEN
            dbms_output.put_line(sqlerrm);
    END update_salary;

    PROCEDURE update_dept (
        e_id     employees.employee_id%TYPE,
        e_dept   employees.department_id%TYPE
    ) AS
    BEGIN
        UPDATE employees
        SET
            department_id = e_dept
        WHERE
            employee_id = e_id;

        COMMIT;
    EXCEPTION
        WHEN no_data_found THEN
            dbms_output.put_line('The entered employee does not exist!');
        WHEN OTHERS THEN
            dbms_output.put_line(sqlerrm);
    END update_dept;

    FUNCTION return_salary (
        e_id IN employees.employee_id%TYPE
    ) RETURN employees.salary%TYPE AS
        e_salary employees.salary%TYPE;
    BEGIN
        SELECT
            salary
        INTO e_salary
        FROM
            employees
        WHERE
            employee_id = e_id;

        RETURN e_salary;
    EXCEPTION
        WHEN no_data_found THEN
            dbms_output.put_line('The entered employee does not exist!');
        WHEN OTHERS THEN
            dbms_output.put_line(sqlerrm);
    END return_salary;

END widgetltd_maintenance;
/